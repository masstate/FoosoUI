"use strict"
/* 组件：开关 */
$.fn.switcher=function(){
	var element=document.createElement('button'),html,func;
	if ($(this).attr('data-switcher')==='done') return;
	html='<span data-change="开" class="';
	if (this.checked) html+='on';
	else html+='off';
	html+='">关</span>';
	$(element).addClass('switcher');
	$(element).html(html);
	$(this).hide();
	$(this).before(element);
	$(this).attr('data-switcher','done');
	$(element).click(function(){
		var btn=$(this).find('span');
		var change=btn.attr('data-change');
		btn.toggleClass('off');
		if (btn.attr('class')=='off') {
			$(this).next()[0].checked=false;
			btn.attr('data-change',btn.html());
			btn.html(change);
		} else {
			$(this).next()[0].checked=true;
			btn.attr('data-change', btn.html());
			btn.html(change);
        }
	});
}
/* 标题栏和菜单 */
$('.back').click(function(){
	var to=$(this).attr('data-to');
	if (to==='url') {
		window.location.href=$(this).attr('data-url');
	}
});
$('.btn-menu').bind('click',function(){
	var menu=$($(this).attr('data-menu')),func;
	func=function(){
		$(menu).css({"left":"-1000px"});
		$('#bg').unbind('click');
		$('#bg').hide();
	};
	menu.css({"left":"0"});
	$('#bg').show();
	$('#bg').click(function(){func();});
});
$('.menu').find('.item a').bind('click',function(){
	$(this).find('.fa').toggleClass('fa-angle-left').toggleClass('fa-angle-down');
	if ($(this).parents('.item').attr('data-toggle')==='dropdown') { //展开列表
		$(this).parents('.item').children('ul').toggle();
	}
});